harga_pulpen = 3000
harga_pensil = 2000
harga_cortape = 10000

print("Toko ini menjual :")
print("1. Pulpen :", harga_pulpen, "/pcs")
print("2. Pensil :", harga_pensil, "/pcs")
print("3. Correction Tape :", harga_cortape, "/pcs")
print()

print("Masukkan jumlah barang yang ingin Anda beli")
jumlah_pulpen = int(input("Pulpen :"))
jumlah_pensil = int(input("Pensil :"))
jumlah_cortape = int(input("Correction Tape :"))

Total = (jumlah_pulpen * harga_pulpen) + (jumlah_pensil * harga_pensil) + (jumlah_cortape * harga_cortape)
print()

print("Ringkasan Pembelian")
print(f"{jumlah_pulpen} Pulpen x {harga_pulpen}")
print(f"{jumlah_pensil} Pensil x {harga_pensil}")
print(f"{jumlah_cortape} Correction Tape x {harga_cortape}") 
print()

print("Total Pembelian :", Total)
