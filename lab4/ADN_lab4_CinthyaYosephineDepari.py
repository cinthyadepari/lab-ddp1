nama = input("Masukkan nama file input :")
out = input("Masukkan nama file output :")
print()

baris = 0
coba = 0
try:
    with open(nama, "r") as file_in:
        with open(out, 'w') as file_out:
            lines = file_in.readlines()

            for i in range(len(lines)): #iterasi per baris
                terserah = ''
                hasil = 0
                words = lines[i].split()
                coba +=1

                for j in range(len(words)): #iterasi kata
                    word = words[j]

                    for k in range(len(word)): #iterasi huruf
                        kata = word[k]
                        if kata.isnumeric():
                            hasil += int(kata)
                        elif kata.isalpha() or kata == " ":
                            terserah += kata
                    terserah += ' '
                if hasil%2 == 0:
                    baris +=1
                    print(terserah, file=file_out)
    print("Total baris dalam file input :%d"%coba)
    print("Total baris yang disimpan :%d"%baris)

except FileNotFoundError:
    print("Nama file yang Anda masukkan tidak dapat ditemukan :(")