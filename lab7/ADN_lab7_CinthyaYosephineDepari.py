def cetak_daftar_perintah():
	print("Berikut adalah daftar perintah yang dapat anda lakukan")
	print("1. Mengecek apakah x adalah elemen ke N dari suatu list L")
	print("2. Mengecek apakah x merupakan elemen dari suatu list L")
	print("3. Mengecek apakah 2 buah List memiliki urutan yang saling berkebalikan")
	print("4. Mengecek apakah sebuah List Palindrome, yaitu memiliki urutan yang sama ketika dibaca dari kanan maupun kiri")
	print("5. Mendapatkan semua elemen unik dari sebuah List")
	print("9. Exit()")
	return input("Masukkan nomor perintah saja:\n")

def input_list():
	return input("Masukkan semua elemen dari L yang dipisahkan oleh karakter spasi \nMisalnya ['2','a','?'], maka masukkan 2 a ?\n").split()

def cetak_perintah1():
	print("Anda memilih perintah 1, yaitu: ")
	print("1. Mengecek apakah x adalah elemen ke N dari suatu list L")
	print("Asumsikan x dan semua elemen di L memiliki tipe string")
	x = input("Masukkan x: ")
	L = input_list()
	n = int(input("Masukkan nilai N (1<=N<=Banyak elemen L):"))
	while n>len(L) or n<=0:
		print("Nilai N>Banyak elemen L atau kurang dari 1")
		n = int(input("Masukkan kembali nilai N (1<=N<=Banyak elemen L):"))

	hasil = check_nth_element_of_list(x, L, n-1)
	if hasil:
		print(f"{x} adalah elemen ke {n} dari List {L}")
	else:
		print(f"{x} bukan elemen ke {n} dari List {L}")

def cetak_perintah2():
	print("Anda memilih perintah 2, yaitu: ")
	print("2. Mengecek apakah x merupakan elemen dari suatu list L")
	print("Asumsikan x dan semua elemen di L memiliki tipe string")
	x = input("Masukkan x: ")
	L = input_list()

	hasil = member(x,L)
	if hasil:
		print(f"{x} adalah elemen dari List {L}")
	else:
		print(f"{x} bukan elemen dari List {L}")

def cetak_perintah3():
	print("Anda memilih perintah 3, yaitu: ")
	print("3. Mengecek apakah 2 buah List memiliki urutan yang saling berkebalikan")
	print("Asumsikan  semua elemen di kedua list memiliki tipe string")
	
	L1 = input_list()
	L2 = input_list()

	hasil = reverse(L2,L1)
	if hasil:
		print(f"{L1} adalah reverse dari {L2} ")
	else:
		print(f"{L1} bukan reverse dari {L2} ")

def cetak_perintah4():
	print("Anda memilih perintah 4, yaitu: ")
	print("4. Mengecek apakah sebuah List Palindrome, yaitu memiliki urutan yang sama ketika dibaca dari kanan maupun kiri")
	print("Asumsikan  semua elemen pada list memiliki tipe string")
	
	L = input_list()
	hasil = palindrome(L)
	if hasil:
		print(f"{L} adalah List Palindrome ")
	else:
		print(f"{L} bukan List Palindrome ")

def cetak_perintah5():
	print("Anda memilih perintah 5, yaitu: ")
	print("5. Mendapatkan semua elemen unik dari sebuah List")
	print("Asumsikan  semua elemen pada list memiliki tipe string")
	LTemp = []
	L = input_list()
	hasil = list_to_set(L,LTemp)

	print(f"{hasil} berisi semua elemen dari list {L} tanpa ada duplikasi ")
	
def new_prepend(L,x):
	res = [x]
	res.extend(L)
	return res

def new_append(L,x):
	L.append(x)
	return L

# Task 1
def check_nth_element_of_list( x, L, n):
	if n==0:
		return x == L[0]
	else:
		return check_nth_element_of_list(x, L[1:],  n-1)

# Task 2
def member(x, L): 
	if L(x) :
		return True
	else:
		return False

# Task 3
def reverse(L1, L2, L3=[]):
	if len(L1)==len(L2):
		return True
    else:
		return False

# Task 4
def palindrome(L): 
	if len(L)==0:
    	return True
	elif len(L)==1:
		return True
	else:
		if(L[0]==L[-1]):
			return palindrome(L[1:-1])
		else:
			return False

# Task 5
def list_to_set(List1, List2=[]):
	if List1 == list():
		return List1 = set()

	elif List2.issuperset(List1):
		return True

    return [] 

def main():
	print("Selamat datang di dunia Prolog")
	cekExit = False
	while not cekExit:
		nomor_perintah  = cetak_daftar_perintah()
		if nomor_perintah == "9":
			print("Anda telah keluar dari dunia Prolog")
			cekExit = True
			return
		elif nomor_perintah == "1":
			cetak_perintah1()
		elif nomor_perintah == "2":
			cetak_perintah2()
		elif nomor_perintah == "3":
			cetak_perintah3()
		elif nomor_perintah == "4":
			cetak_perintah4()
		elif nomor_perintah == "5":
			cetak_perintah5()
		else:
			print(f"nomor masukan {nomor_perintah} tidak terdapat pada daftar")
		print("\n","\n")
	return

main()