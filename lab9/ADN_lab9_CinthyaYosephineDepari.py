karyawan = 0
cash = 0
kue = 0
deterjen = 0
bersih = None

daftar_user = {}
nama = {}

class User:
    def __init__(self, username,):
        self.username = username

    def getUsername(self):
        return self.username

class Kasir(User):
    global cash 

    def __init__(self, username, cash):
        super().__init__(username)
        self.cash = cash
        self.Cash = []

    def getcash(self):
        return self.cash

    def addCash(self, cash):
        self.Cash.append(cash)

class Janitor(User):
    global deterjen
    global bersih

    def __init__(self,username, deterjen):
        super().__init__(username)
        self.deterjen = deterjen
        self.Deterjen = []

    def getdeterjen(self):
        return self.deterjen

    def addDeterjen(self, deterjen):
        self.Deterjen.append(deterjen)

class Chef(User):
    def __init__(self, username, kue):
        super().__init__(username)
        self.kue = kue
        self.Kue = []

    def getKue(self):
        return self.kue

    def addKue(self, kue):
        self.Kue.append(kue)

    def minKue(self, min):
        self.kue -= min

def register():
    username = data[1]
    password = data[2]
    nama = data[3]
    role = data[4]

    if (not password.isdigit):
        print("Akun tidak valid")

    else:
        if (username in daftar_user):
            print("Username sudah terdaftar.")
        else:
            daftar_user[username] = Kasir(username, cash)

def log_in():
    username = input("user_name: ")
    if (username not in daftar_user):
        print(f"Akun dengan user_name {username} tidak ditemukan")
        return None
    
    else:
        print(f"Anda telah masuk dalam akun {username} sebagai {daftar_user[username].getTipe()}")
        return daftar_user[username]

print("Selamat datang di Sistem Manajemen Homura")
print()

while True:
    print("Apa yang ingin anda lakukan? (Tulis angka saja)")
    print("1. Register karyawan baru")
    print("2. Login")
    print("8. Status Report")
    print("9. Karyawan Report")
    print("11. Exit")
    pilih=int(input("Pilihan :"))
    if pilih==1:
        print("Format data: [username]  [password] [nama] [umur] [role]")
        data = input("Input data karyawan baru:")
        print("Karyawan"+ [nama] +"berhasil ditambahkan")
    elif pilih==2:
        log_in()
    elif pilih==8:
        print("="*35)
        print("STATUS TOKO HOMURA SAAT INI")
        print("Jumlah Karyawan: %d"%karyawan)
        print("Jumlah Cash: %d"%cash)
        print("Jumlah Kue: %d"%kue)
        print("Jumlah deterjen: %d"%deterjen)
        print("Terakhir kali dibersihkan: %s"%bersih)
        print("="*35)
    elif pilih==11:
        print("TERIMA KASIH TELAH MENGGUNAKAN SISTEM MANAJEMEN HOMURA")
        quit
    else:
        print("Pilihan Anda tidak valid. Silahkan coba kembali")