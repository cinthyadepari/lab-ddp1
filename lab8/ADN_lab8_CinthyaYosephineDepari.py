class KueUlangTahun:
    def __init__(self, tipe, harga, tulisan, angka_lilin, topping):
        self.__tipe = tipe
        self.__harga = harga
        self.__tulisan = tulisan
        self.__angka_lilin = angka_lilin
        self.__topping = topping
    
    def get_tipe(self):
        return self.__tipe

    def get_harga(self):
        return self.__harga
    
    def get_tulisan(self):
        return self.__tulisan
    
    def get_angka_lilin(self):
        return self.__angka_lilin
    
    def get_topping(self):
        return self.__topping

class KueSponge(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, rasa, warna_frosting, harga = 2500):
        super().__init__(tulisan, angka_lilin,topping, harga)
        self.__rasa = rasa
        self.__warna_frosting = warna_frosting
        
    def get_rasa(self):
        return self.__rasa

    def get_warna_frosting(self):
        return self.__warna_frosting

class KueKeju(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_keju, harga=3000):
        super().__init(tulisan, angka_lilin,topping, harga)
        self.jenis_kue_keju = jenis_kue_keju
        self.harga = int(harga)

    def get_jenis_kue_keju(self):
        return self.jenis_kue_keju

class KueBuah(KueUlangTahun):
    def __init__(self, tulisan, angka_lilin, topping, jenis_kue_buah, harga=3500):
        super().__init__(tulisan, angka_lilin, topping, harga)
        self.jenis_kue_buah = jenis_kue_buah
        self.harga = int(harga)

    def get_jenis_kue_buah(self):
        return self.jenis_kue_buah

def buat_custom_bundle():
    print("Jenis kue :")
    print("1. Kue Sponge")
    print("2. Kue Keju")
    print("3. Kue Buah")
    print()
    jenis = int(input("Pilih tipe kue:"))
    buah = input("Pilih jenis kue buah (American/British):")
    topping2 = input("Pilih topping (Ceri/Stroberi):")
    usia = int(input("Masukkan angka lilin:"))
    tulis = input("Masukkan tulisan pada kue:")

def pilih_premade_bundle():
    print("Pilihan paket istimewa :")
    print("1. New York-style Cheesecake with Strawberru Topping")
    print("2. Chocolate Sponge Cake with Cherry Topping and Blue Icing")
    print("3. American Fruitcake with Apple-Grape-Melon-mix Topping")
    print()
    paket = int(input("Pilih paket:"))
    angka = int(input("Masukkan angka lilin:"))
    tulisan_kue = input("Masukkan tulisan pada kue :")

def print_detail_kue(kue):
    print((jenis) + (buah) + 'dengan topping' + (topping2))
    print("Tulisan ucapan yang anda inginkan adalah" + "" + (tulisan_kue))
    print("Angka lilin yang anda pilih adalah" + (angka))
    print("Harga:%d"%harga)

def main():
    print("Selamat datang di Homura!")
    print("Saat ini sedang diadakan event khusus bertema kue ulang tahun.")

    is_ganti = True

    while is_ganti:
        print("\nBundle kue yang kami sediakan: ")
        print("1. Bundle pre-made")
        print("2. Bundle custom\n")

        pilihan_bundle = input("Pilih bundle: ")

        kue = None

        while True:
            if pilihan_bundle == "1":
                kue = pilih_premade_bundle()
                break
            elif pilihan_bundle == "2":
                kue = buat_custom_bundle()
                break
            else:
                print("Pilihan anda tidak valid.")
                pilihan_bundle = input("Pilih paket: ")
        
        print("\nBerikut adalah kue pesanan anda: ")

        print_detail_kue(kue)

        while True:
            ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")

            if ganti == "Ya":
                break
            elif ganti == "Tidak":
                is_ganti = False
                break
            else:
                print("Pilihan anda tidak valid.")
                ganti = input("Apakah anda ingin mengganti pesanan anda? (Ya/Tidak) ")
    
    print("\nTerima kasih sudah berbelanja di Homura!")

if __name__ == "__main__":
    main()